// i/o example

#include <iostream>
using namespace std;

int main()
{
    int i;
    cout << "please enter an integer value: ";
    cin >> i;
    cout << "the value you entered is" << i;
    cout << " and its double is " << i*2 << ".\n";
    return 0;
}
